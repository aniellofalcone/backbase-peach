'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /peachTransitions when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/peachTransitions");
  });


  describe('peachTransitions', function() {

    beforeEach(function() {
      browser.get('index.html#!/peachTransitions');
    });


    // it('should render peachTransitions when user navigates to /peachTransitions', function() {
    //   expect(element.all(by.css('[ng-view] p')).first().getText()).
    //     toMatch(/partial for view 1/);
    // });

  });

});
