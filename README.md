# `backbase-peach`

## Setup
Hi everyone! I hope you'll enjoy this project.

To run it you need to clone it from here and run in the terminal "npm start"
while you are in the project folder, it should run everything by himself.
After that you can see it at "http://localhost:8000".

## Explanations
What I did was to try to use just necessary external libraries, to try to be
loyal to the Material Design and to be as clean as possible.

You can see, I've just used "ngMaterial", "ui.bootstrap" and "ng-currency"
modules and "fontawesome" icons for the ordering ones.
I needed ngMaterial to use all the components used in this project such as
md-input or md-toolbar.

Instead I used ui.bootstrap to implement bootstrap grid system, which helped me
with the correct size ratio even in a responsiveness environment.

Then, I used ng-currency to correctly display a currency symbol and a correct
number format after the user prompts a number in the amount input of the
transfer section.

That's it.

The requirements should be all satisfied and I'm very excited to had such a
chance with Backbase.

I hope you like it and I hope to be part of your future!
Thanks very much.

Aniello Falcone
