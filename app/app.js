'use strict';

// Declare app level module which depends on views, and components
angular.module('backPeach', [
  'ngRoute',
  'backPeach.peachTransitions',
  'ngMaterial',
  'ngAnimate',
  'ngAria',
  'ngMessages',
  'ui.bootstrap',
  'ng-currency'
])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/peachTransitions'});
}]);
