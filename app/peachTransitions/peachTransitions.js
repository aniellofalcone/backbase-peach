// jshint esversion: 6
'use strict';

angular.module('backPeach.peachTransitions', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/peachTransitions', {
    templateUrl: 'peachTransitions/peach-transitions.html',
    controller: 'peachTransitionsCtrl'
  });
}])

.controller('peachTransitionsCtrl', ["$scope", "$filter", "$mdDialog", "$http", "orderByFilter",
  function($scope, $filter, $mdDialog, $http, orderBy) {

  // CHECK IF MOBILE TO HIDE MERCHANT LOGO ON MOBILE
  $scope.isMobile = (function() {
    if(window.innerWidth <= 800 && window.innerHeight <= 600) {
      return true;
    } else {
      return false;
    }
  })();
  $scope.transaction  = {
    fromAccount   : {
      title       : "From account",
      accountName : "Free Checking",
      accountNumb : "4692",
      balance     : 5824.76,
      isFromInput : true
    },
    toAccount     : {
      title: "To account",
      value: ""
    },
    amount        : {
      title: "Amount",
      value: 0
    },
  };
  $scope.history      = [];
  $scope.sortButtons  = {
    date        : false,
    beneficiary : false,
    amount      : false
  };

  $scope.cleanAmount  = function() {
    $scope.transaction.amount.value = "";
  };
  $scope.checkBalance = function() {
    if($scope.transaction.fromAccount.balance >= -500){
      $scope.showConfirm();
    } else {
      $scope.negativeBal();
    }
  };

  $scope.showConfirm  = function() {

    var confirm = $mdDialog.confirm()
          .title('Confirm transaction')
          .textContent('Are you sure you want to confirm this transaction?')
          .ok('Confirm')
          .cancel('Cancel');

    $mdDialog.show(confirm).then(function() {
      $scope.history.unshift({
        "amount": $scope.transaction.amount.value,
        "categoryCode": "#fbbb1b",
        "merchant": $scope.transaction.toAccount.value,
        "merchantLogo": "/assets/small_logo.png",
        "transactionDate": new Date(),
        "transactionType": "Online Transfer"
      });

      $scope.transaction.fromAccount.balance -= $scope.transaction.amount.value;
      $scope.transaction.toAccount.value      = "";
      $scope.transaction.amount.value         = 0;
    }, function() {});
  };
  $scope.negativeBal  = function() {
    var alert = $mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Insufficient funds')
      .textContent('We are sorry but your actual amount does not allow you to do a new transaction')
      .ok('Close');

    $mdDialog.show(alert).then(function(){
      $scope.transaction.toAccount.value      = "";
      $scope.transaction.amount.value         = 0;
    });
  };

  // DEFAULT ORDERBY PARAMS
  $scope.propertyName = 'transactionDate';
  $scope.reverse = true;
  $scope.history = orderBy($scope.history, $scope.propertyName, $scope.reverse);
  $scope.buttonIcons={
    transactionDate : {
      asc : true,
      desc: false
    },
    amount          : {
      asc : false,
      desc: false
    },
    merchant        : {
      asc : false,
      desc: false
    }
  };

  $scope.sortBy     = function(propertyName) {
    // SORT ICON CONTROLLER
    switch (propertyName) {
      case 'transactionDate':
        if(!$scope.reverse){
          $scope.buttonIcons={
            transactionDate : {
              asc : true,
              desc: false
            },
            amount          : {
              asc : false,
              desc: false
            },
            merchant        : {
              asc : false,
              desc: false
            }
          };
        } else {
          $scope.buttonIcons={
            transactionDate : {
              asc : false,
              desc: true
            },
            amount          : {
              asc : false,
              desc: false
            },
            merchant        : {
              asc : false,
              desc: false
            }
          };
        }
        break;
      case 'amount':
      if(!$scope.reverse){
        $scope.buttonIcons={
          transactionDate : {
            asc : false,
            desc: false
          },
          amount          : {
            asc : true,
            desc: false
          },
          merchant        : {
            asc : false,
            desc: false
          }
        };
      } else {
        $scope.buttonIcons={
          transactionDate : {
            asc : false,
            desc: false
          },
          amount          : {
            asc : false,
            desc: true
          },
          merchant        : {
            asc : false,
            desc: false
          }
        };
      }
        break;
      case 'merchant':
        if(!$scope.reverse){
          $scope.buttonIcons={
            transactionDate : {
              asc : false,
              desc: false
            },
            amount          : {
              asc : false,
              desc: false
            },
            merchant        : {
              asc : true,
              desc: false
            }
          };
        } else {
          $scope.buttonIcons={
            transactionDate : {
              asc : false,
              desc: false
            },
            amount          : {
              asc : false,
              desc: false
            },
            merchant        : {
              asc : false,
              desc: true
            }
          };
        }
        break;
      default:
      if(!$scope.reverse){
        $scope.buttonIcons={
          transactionDate : {
            asc : true,
            desc: false
          },
          amount          : {
            asc : false,
            desc: false
          },
          merchant        : {
            asc : false,
            desc: false
          }
        };
      } else {
        $scope.buttonIcons={
          transactionDate : {
            asc : true,
            desc: false
          },
          amount          : {
            asc : false,
            desc: false
          },
          merchant        : {
            asc : false,
            desc: false
          }
        };
      }
    }

    $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
    $scope.history = orderBy($scope.history, $scope.propertyName, $scope.reverse);
  };
  $scope.cleanQuery = function(){
    $scope.query = '';
  };

  // GET DATA FROM MOCK
  $http.get('assets/transactions.json').then(function(response){
    $scope.history = response.data.data;

    // CHANGE AMOUNT FORMAT TO INT
    $scope.history.forEach(function(transaction){
      transaction.amount = Number(transaction.amount);
    });
  });

}]);
